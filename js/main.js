const ball = document.querySelector(".ball");
const count = document.querySelector(".count");

const step = 20;
let valueBall = ball.clientWidth;
let offsetTop = ball.offsetTop;

let newValueBall;
let newTop;
let newLeft;

const maxWidth = window.innerWidth;
const maxHeight = window.innerHeight;

const rainbow = ["red", "orange", "yelow", "green", "blue", "black", "purple"];

let countForColor = 0;

let countClick = 0;

const resizeBall = (e) => {

  if(e.type == 'click') {
    countClick++;
    count.innerHTML = countClick;
  }

  if (countForColor == 7) {
    countForColor = 0;
  }

  //

  newValueBall = valueBall - step;

  ball.style.cssText =
    "width:" +
    newValueBall +
    "px; height:" +
    newValueBall +
    "px; transition: all 0.2s ease-out; background-color:" +
    rainbow[countForColor] +
    ";";

  newTop = Math.floor(
    getRandomInRange(0, maxHeight - offsetTop - newValueBall, 3)
  );
  newLeft = Math.floor(getRandomInRange(0, maxWidth - newValueBall, 3));

  ball.style.marginTop = newTop.toString() + "px";
  ball.style.marginLeft = newLeft.toString() + "px";

  //

  valueBall = newValueBall;
  offsetTop = newTop;

  //

  countForColor++;

 if(newValueBall == 0){
    alert('Вы накликали - ' + countClick);
    location.reload()
 }

};

function getRandomInRange(from, to, fixed) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
}

ball.addEventListener("mouseover", resizeBall);
ball.addEventListener("click", resizeBall);
